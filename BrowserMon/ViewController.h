//
//  ViewController.h
//  BrowserMon
//
//  Created by Doug Mason on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIWebViewDelegate,UITextFieldDelegate>

@property(nonatomic,retain) IBOutlet UITextField* addressText;
@property(nonatomic,retain) IBOutlet UIButton* goButton;
@property(nonatomic,retain) IBOutlet UIWebView* web;
-(IBAction)load;
@end
