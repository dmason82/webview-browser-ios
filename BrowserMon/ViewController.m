//
//  ViewController.m
//  BrowserMon
//
//  Created by Doug Mason on 4/4/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize web;
@synthesize goButton;
@synthesize addressText;
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

-(IBAction)load
{
    [web loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:addressText.text]]];
    [addressText resignFirstResponder];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self load];
    [textField resignFirstResponder];
}
@end
